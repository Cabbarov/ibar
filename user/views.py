from django.shortcuts import render
from .models import *
from .serializers import *
from django.contrib.auth import get_user_model, authenticate
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly, BasePermission
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
User = get_user_model()

class AdminPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated:
            userid = request.user.pk
            comp = CorporativeUser.objects.filter(id=userid).first()
            if comp:
                return True
        else:
            return False

class CreateUserView(CreateAPIView):
    permission_classes = [AllowAny]
    model = User.objects.all()
    serializer_class = CorporativeRegisterSerializer


class CreatePersonalView(APIView):
    permission_classes = [AllowAny]
    model = User.objects.all()
    serializer_class = PersonalRegisterSerializer

    def post(self, request, *args, **kwargs):
        userid = request.user.pk
        comp = CorporativeUser.objects.filter(id=userid).first()

        user = User(
            email = request.data.get('email'),
            username = request.data.get('username'),
            password = request.data.get('password'),
        )

        personal = Personal(
            name =  request.data.get('personals.name'),
            surname = request.data.get('personals.surname'),
            workingplace =  comp,
        )
        
        setattr(self, user, personal)
        
        user.save()

        user.personals.save(personal)
        return JsonResponse({
                'status': HTTP_201_CREATED
            })


