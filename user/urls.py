from django.urls import path
from .views import *
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('token', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('corporativeregister', CreateUserView.as_view(), name='register'),
    path('personalregister/<int:id>', CreatePersonalView.as_view(), name='personalregister'),
]