from django.db import models
from phone_field import PhoneField
from django.contrib.auth.models import AbstractBaseUser, User, BaseUserManager
from taskify.models import *


class CorporativeUser(models.Model):
    username = models.ForeignKey(User, related_name='corporativeuser', on_delete=models.CASCADE)
    organizationname = models.CharField(max_length=255, null=True ,blank=True)
    address = models.CharField(max_length=500, null=True ,blank=True)
    phone = PhoneField(help_text='Contact phone number', null=True, blank=True)
    admin = models.BooleanField(default=True)

    def __str__(self):
        return self.organizationname


class Personal(models.Model):
    username = models.ForeignKey(User, related_name='personals', on_delete=models.CASCADE)
    workingplace = models.ForeignKey(CorporativeUser, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)


    def get_project(self):
        return Task.objects.filter(personal_list__in=[self.pk])


    def __str__(self):
        return self.name
 


