from rest_framework.serializers import (
    CharField,
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
    ChoiceField,
    DateField,
    StringRelatedField,
    DictField, 
    ValidationError  
    
)
from django.contrib.auth import get_user_model
from .models import * 
import django.contrib.auth.password_validation as validators

User = get_user_model()


class CorporativeUserSerializer(ModelSerializer):

    class Meta:
        model = CorporativeUser
        fields = ['organizationname',  'address', 'phone']


class CorporativeRegisterSerializer(ModelSerializer):

    password = CharField(style={'input_type': 'password'}, write_only=True)
    corporativeuser = CorporativeUserSerializer()

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'corporativeuser']

    def validate(self, attr):
        validators.validate_password(attr['password'])
        return attr

    def create(self, validated_data):

        corporative_data = validated_data.pop('corporativeuser')
        emailval = User.objects.filter(email=validated_data['email'])
        if emailval:
            raise ValidationError({'email': 'correct email adress'})

        account = User(
            email = validated_data['email'],
            username = validated_data['username']
        )

        password = validated_data['password']

        account.set_password(password)
        account.save()

        corp = CorporativeUser.objects.create(username=account, **corporative_data)

        return account


class workingplaceseri(ModelSerializer):
    class Meta:
        model = CorporativeUser
        fields = ['name']

class PersonalUserSerializer(ModelSerializer):

    class Meta:
        model = Personal
        fields = ['name', 'surname', 'workingplace']


class PersonalRegisterSerializer(ModelSerializer):

    password = CharField(style={'input_type': 'password'}, write_only=True)
    personals = PersonalUserSerializer(required=True, many=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'personals']

    def validate(self, attr):
        validators.validate_password(attr['password'])
        return attr

    def create(self, validated_data):

        personals = validated_data.pop('personals')
        emailval = User.objects.filter(email=validated_data['email'])
        if emailval:
            raise ValidationError({'email': 'correct email adress'})
        
        account = User(
            email = validated_data['email'],
            username = validated_data['username']
        )

        password = validated_data['password']
        account.set_password(password)
        account.save()
        pers = Personal.objects.create(username=account, **personals)
        return account


