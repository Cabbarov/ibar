from django.contrib import admin
from .models import *

admin.site.register(CorporativeUser)
admin.site.register(Personal)

# Register your models here.
