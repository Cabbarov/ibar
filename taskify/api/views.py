from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly, BasePermission
from .serializers import *
from taskify.models import *
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, \
    HTTP_406_NOT_ACCEPTABLE, HTTP_403_FORBIDDEN
from django.core.paginator import Paginator
from django.core.mail import BadHeaderError, send_mail
from taskify.signals import sendmail
from user.models import Personal



class CreateTasksView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = CreateTaskSerializer

    def post(self, request, *args, **kwargs):
        userid = request.user.pk

        comp = CorporativeUser.objects.filter(id=userid).first()

        if comp:
            title = request.data.get("title")
            description = request.data.get("description")
            deadline = request.data.get("deadline")
            status = request.data.get('status')
            personal_list = request.data.get('personal_list')

            queryset = Personal.objects.filter(id=personal_list).first()



            task = Task(
                company=comp,
                description=description,
                deadline=deadline
            )

            setattr(task, title, status)
            task.save()
            task.personal_list.set([personal_list])

            subject = "New Taskk Added"
            message = title
            from_email = comp.username.email
            tomail = queryset.username.email
            try:
                mail = sendmail(from_email, tomail, subject, message)
            except Exception as err:
                print(err)

            return JsonResponse({
                'status': HTTP_201_CREATED
            })
        else:
            return JsonResponse({
                'status': HTTP_400_BAD_REQUEST
            }, status=400)


class TaskAllView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = TaskViewSerializer
    #queryset = Task.objects.all()
    paginate_by = 10

    def get_queryset(self):
        userid = self.request.user.pk
        return Task.objects.filter(company__username__id=userid)

        


