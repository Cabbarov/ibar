from taskify.models import *
from rest_framework.serializers import (
    DateField,
    Serializer,
    PrimaryKeyRelatedField,
    CharField,
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
    ChoiceField,
    DateField,
    StringRelatedField,
    DictField, 
    ValidationError  
    
)
from django.contrib.auth import get_user_model
import django.contrib.auth.password_validation as validators
from user.models import Personal, CorporativeUser
from core.settings import DATE_INPUT_FORMATS


class CreateTaskSerializer(ModelSerializer):
    deadline = DateField(format=DATE_INPUT_FORMATS, required=True)

    class Meta:
        model = Task
        fields = ['title', 'description', 'deadline','status', 'personal_list']

    def __init__(self, *args, **kwargs):
        user = kwargs['context']['request'].user.pk
        comp = CorporativeUser.objects.filter(id=user).first()
        

        super(CreateTaskSerializer, self).__init__(*args, **kwargs)
        self.fields['personal_list'].queryset = Personal.objects.filter(workingplace=comp)

class TaskViewSerializer(ModelSerializer):

    class Meta:
        model = Task
        fields = ['title', 'description', 'deadline','status', 'personal_list']

    