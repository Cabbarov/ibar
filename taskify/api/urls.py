from django.urls import path
from .views import *

urlpatterns = [
    path('all', TaskAllView.as_view(), name='task'),
    path('create', CreateTasksView.as_view(), name='create')
]