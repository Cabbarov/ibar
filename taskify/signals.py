
from django.core.mail import send_mail 
from django.conf import settings
from django.db.models.signals import post_save
import smtplib
import ssl

def sendmail(fromemail, toemail, subject, body):
    port = settings.EMAIL_PORT
    smtp_server = settings.EMAIL_HOST
    sender_email = fromemail
    password = settings.EMAIL_HOST_PASSWORD
    receiver_email = toemail
    subject = subject
    body = body
    message = 'Subject: {}\n\n{}'.format(subject, body)
    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)
        server.close()
