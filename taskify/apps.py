from django.apps import AppConfig


class TaskifyConfig(AppConfig):
    name = 'taskify'
