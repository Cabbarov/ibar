from django.db import models
from user.models import *

class Task(models.Model):
    company =  models.ForeignKey('user.CorporativeUser', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    deadline = models.DateField()
    status = models.BooleanField(default=True, null=True, blank=True)
    personal_list = models.ManyToManyField('user.Personal')

    def __str__(self):
        return self.description



