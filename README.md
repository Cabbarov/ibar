1. step: git clone https://gitlab.com/Cabbarov/ibar.git
2. step: change directory to project folder to cmd
3. step: pip install -U pip
4. step: pip install requirements.txt
5. step: python manage.py makemigrations user
6. step: python manage.py migrate user
7. step: python manage.py makemigrations taskify
8. step: python manage.py migrate taskify
9. step: python manage.py migrate
10. step: open core.settings.py
11. step: create self gmail account(EMAIL_HOST_USER = 'your mail username @gmail.com') (EMAIL_HOST_PASSWORD = 'Password')
12. step: python manage.py createsuperuser
13. step: python manage.py runserver
14. step: open self internet explorer
15. step: documantation path = (http://127.0.0.1:8000/docs/)
